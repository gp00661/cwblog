import os
from dotenv import load_dotenv

dotenv_path = os.path.join(os.path.dirname(__file__), '.env')
if os.path.exists(dotenv_path):
    load_dotenv(dotenv_path)


class Config:

    SQLALCHEMY_TRACK_MODIFICATONS = False


class DevelopmentConfig(Config):
    ENV = "development"
    DEBUG = True
    SQLALCHEMY_DATABASE_URI = 'sqlite:///posts-service.db'
    # SQLALCHEMY_DATABASE_URI = 'mysql+pymysql://post-service:pass1234@host.docker.internal:3306/post_dev'
    SQLALCHEMY_ECHO = True


class ProductionConfig(Config):
    ENV = "production"
    DEBUG = True
    SQLALCHEMY_DATABASE_URI = 'mysql+pymysql://post-service:pass1234@db:3306/post'
    SQLALCHEMY_ECHO = False
