# application/__init__.py
import config
import os
from flask import Flask
from flask_sqlalchemy import SQLAlchemy

db = SQLAlchemy()

def create_app():
    app = Flask(__name__)

    # environment_configuration = os.getenv('CONFIGURATION_SETUP')
    app.config.from_object(config.DevelopmentConfig)
    
    db.init_app(app)

    with app.app_context():
        # Register blueprints
        from .post_api import post_api_blueprint
        app.register_blueprint(post_api_blueprint)

        from .models import Comment,Post
        db.create_all()
         
        return app
