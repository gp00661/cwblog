# application/post_api/routes.py
from flask import jsonify, request, make_response, abort
from . import post_api_blueprint
from .. import db
from ..models import Post, Comment
from .api.UserClient import UserClient

# for initial post
@post_api_blueprint.route('/api/initial-post')
def post_initialpost():

    post = Post()
    post.user_id = int(1)
    post.title = 'hello surrey'
    post.category = 'social'
    post.content = 'this is initial post, hello world'

    db.session.add(post)
    db.session.commit()
    
    response = jsonify({'message': 'Post added', 'result': post.to_json()})

    return response

# for initial comment
@post_api_blueprint.route('/api/initial-comment')
def post_initialcomment():

    comment = Comment()
    comment.user_id = int(2)
    comment.post_id = int(2) 
    comment.content = 'welcome to comment section'

    db.session.add(comment)
    db.session.commit()
    
    response = jsonify({'message': 'Comment added', 'result': comment.to_json()})

    return response
    
@post_api_blueprint.route('/api/posts', methods=['GET'])
def get_posts():
    data = []
    for row in Post.query.all():
        data.append(row.to_json())

    if data == []:
        abort(404) 

    response = jsonify(data)
    return response

@post_api_blueprint.route('/api/<int:post_id>', methods=['GET'])
def get_post(post_id):
    responses = []

    post = Post.query.get_or_404(post_id)
    responses.append(post.to_json())

    for row in Comment.query.filter(Comment.post_id == post_id).all():
        responses.append(row.to_json())

    if responses == []:
        abort(404)    

    response = jsonify(responses)

    return response

@post_api_blueprint.route('/api/comments', methods=['GET'])
def get_comments():
    data = []
    for row in Comment.query.all():
        data.append(row.to_json())

    if data == []:
        abort(404)  

    response = jsonify(data)
    return response

@post_api_blueprint.route('/api/<int:post_id>/<int:comment_id>', methods=['GET'])
def get_comment(post_id,comment_id):
    comment_response = []

    comment = Comment.query.filter(Comment.post_id == post_id,
        Comment.id == comment_id).first() 

    if comment is None:
        abort(404)

    comment_response.append(comment.to_json()) 

    response = jsonify(comment_response)

    return response

@post_api_blueprint.route('/api/new-post', methods=['POST'])
# @login_required
def post_newpost():

    # user_id = request.form['user']
    user = request.form['current_user']
    u_id = int(user['id'])

    title = request.form['title']
    category = request.form['category']
    content = request.form['content']

    post = Post()
    post.user_id = u_id
    # post.user_id = user_id
    post.title = title
    post.category = category
    post.content = content

    db.session.add(post)
    db.session.commit()

    response = jsonify({'message': 'Post added', 'result': post.to_json()})

    return response

@post_api_blueprint.route('/api/<int:post_id>/delete', methods=['GET','POST'])
# @login_required
def delete_post(post_id):
    post = Post.query.get_or_404(post_id)

    db.session.delete(post)
    db.session.commit()

    response = jsonify({'message': 'Post deleted'})

    return response

# @post_api_blueprint.route('/api/<int:post_id>/new-comment', methods=['POST'])
@post_api_blueprint.route('/api/new-comment', methods=['POST'])
# @login_required
def post_comment():

    user = request.form['current_user']
    u_id = int(user['id'])

    content = request.form['content']
    post_id = request.from_object['post_id']

    comment = Comment()
    comment.user_id = u_id
    comment.post_id = post_id
    comment.connect = content

    db.session.add(comment)
    db.session.commit()

    response = jsonify({'message': 'Comment added', 'result': comment.to_json()})

    return response


@post_api_blueprint.route('/api/<int:post_id>/<int:comment_id>/delete', methods=['GET','POST'])
# @login_required
def delete_comment(post_id,comment_id): 
 
    comment = Comment.query.filter(Comment.post_id == post_id,
        Comment.id == comment_id).first()

    db.session.delete(comment)
    db.session.commit()

    response = jsonify({'message': 'Comment deleted'})

    return response
