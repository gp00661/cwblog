# application/__init__.py
from flask import Flask
from os import environ
from flask_login import LoginManager

login_manager = LoginManager()


def create_app():
    app = Flask(__name__)

    app.config['SECRET_KEY'] = '3834j2724827'

    login_manager.init_app(app)

    with app.app_context():
    	
        from .frontend import frontend_blueprint
        app.register_blueprint(frontend_blueprint)

        return app
