# application/frontend/forms.py
from flask_wtf import FlaskForm
from flask_wtf.file import FileField, FileAllowed
from flask_login import current_user
from wtforms import (StringField, PasswordField,
                     SubmitField, SelectField)
from wtforms.validators import (InputRequired, Email, EqualTo,
                                ValidationError, Length)
from flask_ckeditor import CKEditorField
# from application.models import User

class CreatePostForm(FlaskForm):
    '''Create post form'''

    title = StringField('Title', validators=[InputRequired()])
    category = SelectField('Category', choices=[
                            ('help', 'Help'),
                            ('python', 'Python'),
                            ('nodejs', 'Nodejs'),
                            ('general', 'General'),
                            ('feedback', 'Feedback'),
                            ('html-css', 'HTML CSS'),
                            ('support', 'Support'),
                            ('javascript', 'Javascript')
                            ])
    content = CKEditorField('Content', validators=[
                            InputRequired(), Length(min=20)])
    submit = SubmitField('CREATE POST')

class CommentForm(FlaskForm):
    '''Comment post form'''

    content = CKEditorField('Comment', validators=[InputRequired()])
    submit = SubmitField('SUBMIT')