# application/frontend/views.py
from flask_wtf import FlaskForm
import requests
from . import forms
from . import frontend_blueprint
from .. import login_manager
# from .api.UserClient import UserClient
from .api.PostClient import PostClient
from flask import render_template, session, redirect, url_for, flash, request, abort
from flask_login import current_user

@frontend_blueprint.route('/', methods=['GET'])
#it is going to change to home page, which categories will be seen in
def get_posts():
    posts = PostClient.get_posts()

    if posts == 404:
        abort(404)
    return posts
    

@frontend_blueprint.route('/post/new', methods=['GET','POST'])
# @login_required
def create_post():
    form = forms.CreatePostForm(request.form)
    if request.method == "POST":
        if form.validate_on_submit():
            post = PostClient.create_post(form)
            if post:
                flash('Post created successfully', 'success')
                return redirect(url_for('forum.create_post', _external=True))
            else :
                flash('Post not successful', 'fail')
                return redirect(url_for('forum.forum_route', _external=True))


@frontend_blueprint.route('/post/<int:post_id>', methods=['GET','POST'])
# @login_required
def display_post(post_id):

    response = PostClient.get_post(post_id)

    if response == 404:
        abort(404)

    post = response[0]
    comments = response[1:]
    form = forms.CommentForm(request.form)

    if request.method == "POST":
        if form.validate_on_submit():
            comment = PostClient.create_comment(form, post_id)
            if comment:
                flash('Comment created successfully', 'success')
                return redirect(url_for('forum.display_post', post_id=post.id))
            else:
                flash('Comment not successful', 'fail')
                return redirect(url_for('forum.display_post', form=form, post_id=post.id))                
    
    content = {
        'post': post,
        'form': form,
        'comments': comments
        }

    return render_template('forum/post.html', **content)


@frontend_blueprint.route('/post/<int:post_id>/<int:comment_id>/delete', methods=['GET', 'POST'])
# @login_required
def delete_comment(post_id,comment_id):
    user_id = 2
    comment = PostClient.get_comment(post_id,comment_id)

    if comment == 404:
        abort(404)

    comment_user_id = comment[0]['user_id']

    # if comment_user_id != current_user:
    if comment_user_id != user_id:
        abort(403)

    delete_comment = PostClient.delete_comment(post_id, comment_id)

    if delete_comment:
        flash(' Your comment has been deleted', 'success')
    else:
        flash(' Comment deletion not successful', 'fail')

    return redirect(url_for('forum.forum_route'))


@frontend_blueprint.route('/post/<int:post_id>/delete', methods=['GET', 'POST'])
# @login_required
def delete_post(post_id):
    user_id = 1
    post = PostClient.get_post(post_id)

    if post == 404:
        abort(404)

    post_user_id = post[0]['user_id']

    # if post_user_id != current_user:
    if post_user_id != user_id:
        abort(403)

    delete_post = PostClient.delete_post(post_id)
    if delete_post:
        flash(' Your post has been deleted', 'success')
    else:
        flash(' Post deletion not successful', 'fail')

    return redirect(url_for('forum.forum_route'))


# @frontend_blueprint.route('/post/new', methods=['GET','POST'])
# # @login_required
# def create_post():
#     # form = forms.CreatePostForm()
#     # if request.method == "POST":
#     #     if form.validate_on_submit():
#     form = FlaskForm(meta={'csrf': False})
#     form.title = 'hello world v2'
#     form.category = 'social'
#     form.content = 'testing from frontend'
#     form.user_id = 1

#     post = PostClient.create_post(form)
#     print(post)
#     if post:
#         print('Post created successfully', 'success')
#                 # return redirect(url_for('forum.create_post', _external=True))