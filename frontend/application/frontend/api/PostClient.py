# application/frontend/api/PostClient.py
import requests

class PostClient:

    def get_posts():        
        url = 'http://172.16.238.130:5002/api/posts'
        response = requests.request(method="GET", url=url)

        if response.status_code == 404:
            return response.status_code
            
        response = response.json()

        return response

    def get_post(post_id):
        url = 'http://172.16.238.130:5002/api/' + str(post_id)
        response = requests.request(method="GET", url=url)

        if response.status_code == 404:
            return response.status_code

        response = response.json()

        return response

    def get_comment(post_id, comment_id):
        url = 'http://172.16.238.130:5002/api/' + str(post_id) + '/' + str(comment_id)
        response = requests.request(method="GET", url=url)

        if response.status_code == 404:
            return response.status_code

        response = response.json()

        return response


    def create_post(form):
        payload = {'title':form.title.data, 'category':form.category.data,
                        'content':form.content.data, 'user':current_user}

        url = 'http://172.16.238.130:5002/api/new-post'
        response = requests.request(method="POST", url=url, data=payload)

        if response:
            return response

    # def create_post(form):
    #     payload = {'title':form.title, 'category':form.category,
    #                     'content':form.content, 'user':form.user_id}

    #     url = 'http://172.16.238.130:5002/api/new-post'
    #     response = requests.request(method="POST", url=url, data=payload)

    #     if response:
    #         return response

    def delete_post(post_id):

        url = 'http://172.16.238.130:5002/api/' + str(post_id) + '/delete'
        response = requests.request(method="POST", url=url)

        if response:
            return response

    def create_comment(form, post_id):
        payload = {'content':form.content.data, 'user':current_user, 
                'post_id':post_id}

        url = 'http://172.16.238.130:5002/api/new-comment'
        response = requests.request(method="POST", url=url, data=payload)

        if response:
            return response

    def delete_comment(post_id,comment_id):

        url = 'http://172.16.238.130:5002/api/' + str(post_id) + '/' + str(comment_id) + '/delete'
        response = requests.request(method="POST", url=url)

        if response:
            return response
